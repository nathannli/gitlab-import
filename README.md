# Gitlab Issue Import
Gitlab issue export feature was introduced in v12.10 (https://docs.gitlab.com/ee/user/project/issues/csv_export.html). As a result for users of Gitlab versions prior to then, exporting issues from Gitlab and then importing them to current versions of Gitlab requires some scripting. This is especially necessary when the import/export version is incompatible.

# Requirements
- python 3
- requests library
- csv library
- gitlab api

# Usage
This project assumes that the project already exists in the new gitlab, which can be easily done via import using repo URL.

## Variables to modify
`private_token` - to store your private token. For the old gitlab, you get the token by going to your profile settings and finding private token. For the new gitlab, you go to your profile settings and create a personal access token.<br>
`gitlab_home_url` - your own gitlab home link<br>
`group_nm` - modify if you use a group<br>
`project_name` - your project path name. Copy this off of the url from your project.<br>

**note: you will need to do additional modification if the project is located under your own repo. Check the Gitlab API**


## 1) `get_old_gitlab.py` 
Queries the old gitlab and gets all the issues, comments, labels milestones, status of each issue. Issues are written out to `import_this.csv`. This file can be utilized by the new gitlab's **issue import by csv** feature. 

Labels are written out to `labels.txt`, milestones are written out to `milestone.txt`. 

A new folder will be generated called `comments`, where it will contain a folder for each issue. Each issue with have a text file called `comments.txt` that contains the original author name and then the comment on each line, and `status.txt` that contains `open` or `closed` to indicate issue status, and a file called `labels.txt` that contains that issue's labels, and `milestone.txt` that contains that issue's milestones. 

## 2) `import_this.csv`
Go to the new gitlab and to your project -> issues. There you will have the option to import issues. Import issues using the csv file.

If there are errors, it is likely due to unique character problems. You'll need to add extra `.replace()` on line 55 for whatever other unique characters your issues contain.

## 3) `put_labels_milestone.py`
This script reads `labels.txt` and `milestone.txt` to input the labels and milestones into the project in the new gitlab

## 4) `get_new_gitlab.py` 
This script reads the data and pushes the comments, labels and milestones to each issue to the new gitlab. Afterwards, if the issue was closed, it will close the issue, otherwise it will leave open.

# Helper Scripts
## `delete_comments_new_gitlab.py`
This script deletes all comments in the designated project in the new gitlab, whether they are open or closed. This is useful if you need to reimport the comments again and need all the issues to be empty.

## `delete_labels_milestone.py`
This script deletes all labels and milestones from the designated project in the new gitlab. This is useful if you need to clean up the labels & milestones in preparation for a fresh import.

# References Used:
https://gitlab.com/emobix/get-all-gitlab-issues-as-csv

