import requests
import json
import urllib3
import time
import sys


########################## MODIFY THESE ##########################
private_token = "ENTER YOUR PERSONAL ACCESS TOKEN HERE"
new_gitlab_home_url = "https://gitlab.com"                              # modify gitlab.com to the link to where you host your gitlab
group_nm = "ENTER YOUR GROUP NAME HERE"                                # change to "" if you don't use groups
project_name = "ENTER YOUR PROJECT PATH NAME HERE"
##################################################################

headers = {"PRIVATE-TOKEN":private_token}
base_url = new_gitlab_home_url + "/api/v4"
if group_nm != "":
    group_url = "/groups/" + group_nm
else:
    group_url = ""

print("Start")
start_time = time.time()

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


# get project_id
project_url = "/projects/"
perpage_parameters = {"per_page":"100"}

response = requests.get(base_url + group_url + project_url, headers=headers, params=perpage_parameters, verify=False)
parsed = json.loads(response.text)

links = None
project_id = -1
while True:
    for projects in parsed:
        if projects['path'] == project_name:
            project_id = projects['id']
            links = projects['_links']
            break
    if 'next' in response.links.keys():
        response = requests.get(response.links['next']['url'], headers=headers, params=perpage_parameters, verify=False)
        parsed = json.loads(response.text)
    else:
        if project_id == -1:
            print("project cannot be found")
            exit()
        break

project_url = "/projects/"+ str(project_id)


# print("Opening issues")
# # start open issues

# issues_response = requests.get(links['issues'], headers=headers, params=perpage_parameters, verify=False)
# issues_parsed = json.loads(issues_response.text)

# open_parameters = {"state_event":"reopen"}

# while True:
#     for issue in issues_parsed:
#         issue_url = "/issues/" + str(issue['iid'])
#         print("opening " + issue_url)
#         put_response = requests.put(base_url + project_url + issue_url, headers=headers, data=open_parameters, verify=False)
#         if not put_response.ok:
#             print(issue_url)
#             raise Exception
#     if 'next' in issues_response.links.keys():
#         issues_response = requests.get(issues_response.links['next']['url'], headers=headers, verify=False)
#         issues_parsed = json.loads(issues_response.text)
#     else:
#         break


print("Deleting comments")
# start delete comments for issues
issues_response = requests.get(links['issues'], headers=headers, params=perpage_parameters, verify=False)
issues_parsed = json.loads(issues_response.text)

while True:
    for issue in issues_parsed:
        notes_link = issue['_links']['notes']
        issue_url = "/issues/" + str(issue['iid'])
        print("deleting comments for " + issue_url)
        notes_response = requests.get(notes_link, headers=headers, params=perpage_parameters, verify=False)
        notes_parsed = json.loads(notes_response.text)
        while True:
            for note in notes_parsed:
                notes_url = "/notes/" + str(note['id'])
                del_response = requests.delete(base_url + project_url + issue_url + notes_url, headers=headers, verify=False)
            if 'next' in notes_response.links.keys():
                notes_response = requests.get(notes_response.links['next']['url'], headers=headers, verify=False)
                notes_parsed = json.loads(notes_response.text)
            else:
                break
    if 'next' in issues_response.links.keys():
        issues_response = requests.get(issues_response.links['next']['url'], headers=headers, verify=False)
        issues_parsed = json.loads(issues_response.text)
    else:
        break

print("--- %s seconds ---" % (time.time() - start_time))