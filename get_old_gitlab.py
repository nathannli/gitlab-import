import requests
import json
import urllib3
import time
import os
import csv


'''
Assumptions:
Based OS is Windows
python3
* tested on python 3.9.4 *
'''


########################## MODIFY THESE ##########################
private_token = "ENTER YOUR PRIVATE TOKEN HERE"
old_gitlab_home_url = "https://gitlab.com"                              # modify gitlab.com to the link to where you host your gitlab
group_nm = "ENTER YOUR GROUP NAME HERE"                                 # change to "" if you don't use groups
project_name = "ENTER YOUR PROJECT PATH NAME HERE"
##################################################################

headers = {"PRIVATE-TOKEN":private_token}
base_url = old_gitlab_home_url + "/api/v3"
if group_nm != "":
    group_url = "/groups/" + group_nm
else:
    group_url = ""

print("Start comment file generation")
start_time = time.time()

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

comments_url = "/notes"
issues_url = "/issues"
issue_parameters = {"per_page":"100", "sort":"asc"}
comment_parameters = {"per_page":"100"}
issue_cols = ["title", "description", "labels", "state", "assignee", "author", "created_at"]
comment_cols = ["author", "created_at", "body"]
csv_cols = ["title", "description"]

# get project_id
project_url = "/projects"
perpage_parameters = {"per_page":"100"}
response = requests.get(base_url + group_url + project_url, headers=headers, params=perpage_parameters, verify=False)
parsed = json.loads(response.text)

project_id = -1
project_info = None
while True:
    for projects in parsed:
        if projects['path'] == project_name:
            project_id = projects['id']
            project_info = projects
            break
    if 'next' in response.links.keys():
        response = requests.get(response.links['next']['url'], headers=headers, params=perpage_parameters, verify=False)
        parsed = json.loads(response.text)
    else:
        if project_id == -1:
            print("project cannot be found")
            exit()
        break

issue_links = None
issue_id = 0
project_url = "/projects/"+ str(project_id)


# get project labels
labels_url = "/labels"
labels_response = requests.get(base_url + project_url + labels_url, headers=headers, params=perpage_parameters, verify=False)
labels_parsed = json.loads(labels_response.text)
with open("labels.txt", 'w', encoding="utf-8") as outfile:
    for i in labels_parsed:
        outfile.write(i['name'] + ", " + i['color'] + "\n")


# get milestones
milestone_url = "/milestones"
milestone_response = requests.get(base_url + project_url + milestone_url, headers=headers, params=perpage_parameters, verify=False)
milestone_parsed = json.loads(milestone_response.text)
with open("milestone.txt", 'w', encoding="utf-8") as outfile:
    for i in milestone_parsed:
        outfile.write(i['title'] + "\n")


#start parsing issues
response = requests.get(base_url + project_url + issues_url, headers=headers, params=issue_parameters, verify=False)
parsed = json.loads(response.text)

os.system("mkdir comments")

# loop until no more next page
while True:
    for issue in parsed:
        issue_num = issue['iid']
        issue_id = issue['id']
        issue_response = requests.get(base_url + project_url + issues_url + "/" + str(issue_id) + comments_url, headers=headers, params=comment_parameters, verify=False)
        issue_parsed = json.loads(issue_response.text)
        os.system("mkdir comments\\" + str(issue_num))
        comment_list = []
        # while there is another page of comments
        comment_list = []
        while True:
            for comment in issue_parsed:
                stringdata =  comment['body'].replace("•", "- ").replace("–","-").replace("’","'").replace("‘","'").replace(" ","").replace("é","e").replace("É","E")
                if stringdata.startswith("Mentioned in issue") or stringdata.startswith("Milestone changed to") or stringdata.startswith("Reassigned to") or stringdata.startswith("Status changed to") or "labels" in stringdata or "label" in stringdata:
                    continue
                comment_data = [comment['id'], comment['author']['name'], stringdata]
                comment_list.append(comment_data)
            if 'next' in issue_response.links.keys():
                issue_response = requests.get(issue_response.links['next']['url'], headers=headers, params=comment_parameters, verify=False)
                issue_parsed = json.loads(issue_response.text)
            else:
                break
        sorted_comment_list = sorted(comment_list, key=lambda x: x[0])
        j = 0
        while j < len(sorted_comment_list):
            with open("comments/" + str(issue_num) + "/comments" + str(j) + ".txt", 'w', encoding="utf-8") as outfile:
                outfile.write(sorted_comment_list[j][1] + ": " + sorted_comment_list[j][2] + "\n")
            j += 1
        
        # status of issue                    
        with open("comments/" + str(issue_num) + "/status.txt", 'w', encoding="utf-8") as outfile:
            if issue['state'] == 'closed':
                outfile.write("closed")
            else:
                outfile.write("open")
        with open("comments/" + str(issue_num) + "/labels.txt", 'w', encoding="utf-8") as outfile:
            for i in issue['labels']:
                outfile.write(i + "\n")
        with open("comments/" + str(issue_num) + "/milestone.txt", 'w', encoding="utf-8") as outfile:
            try:
                outfile.write(issue['milestone']['title'])
            except TypeError:
                pass
    if 'next' in response.links.keys():
        response = requests.get(response.links['next']['url'], headers=headers, params=issue_parameters, verify=False)
        parsed = json.loads(response.text)
    else:
        break

print("--- %s seconds ---" % (time.time() - start_time))

print("Start csv file generation")
start_time = time.time()

response = requests.get(response.links['first']['url'], headers=headers, params=issue_parameters, verify=False)
parsed = json.loads(response.text)

with open("import_this.csv",'w', newline='\n') as csvfile:
    csv_writer = csv.writer(csvfile, delimiter=',')
    csv_writer.writerow(i for i in csv_cols)
    while True:
        for issue in parsed:
            datarow = []
            for i in csv_cols:
                stringdata =  issue[i].replace("•", " ").replace("–","-").replace("’","'").replace("‘","'").replace(" ","").replace("é","e").replace("É","E")     # may need to add other unique characters
                datarow.append(stringdata)
            csv_writer.writerow(datarow)
        if 'next' in response.links.keys():
            response = requests.get(response.links['next']['url'], headers=headers, params=issue_parameters, verify=False)
            parsed = json.loads(response.text)
        else:
            break

print("--- %s seconds ---" % (time.time() - start_time))