import requests
import json
import urllib3
import time
import sys
import os
import re


########################## MODIFY THESE ##########################
private_token = "ENTER YOUR PERSONAL ACCESS TOKEN HERE"
new_gitlab_home_url = "https://gitlab.com"                              # modify gitlab.com to the link to where you host your gitlab
group_nm = "ENTER YOUR GROUP NAME HERE"                                # change to "" if you don't use groups
project_name = "ENTER YOUR PROJECT PATH NAME HERE"
##################################################################

headers = {"PRIVATE-TOKEN":private_token}
base_url = new_gitlab_home_url + "/api/v4"
if group_nm != "":
    group_url = "/groups/" + group_nm
else:
    group_url = ""

print("Start comment insertion")
start_time = time.time()

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


# get project_id
project_url = "/projects"
perpage_parameters = {"per_page":"100"}

response = requests.get(base_url + group_url + project_url, headers=headers, params=perpage_parameters, verify=False)
parsed = json.loads(response.text)

links = None
project_id = -1
while True:
    for projects in parsed:
        if projects['path'] == project_name:
            project_id = projects['id']
            links = projects['_links']
            break
    if 'next' in response.links.keys():
        response = requests.get(response.links['next']['url'], headers=headers, params=perpage_parameters, verify=False)
        parsed = json.loads(response.text)
    else:
        if project_id == -1:
            print("project cannot be found")
            exit()
        break

project_url = "/projects/"+ str(project_id)


# start comment insertion
issue_links = None
issue_id = 0
note_url = "/notes"
close_issue = {'state_event':'close'}

response = requests.get(links['issues'], headers=headers, params=perpage_parameters, verify=False)
parsed = json.loads(response.text)

while True:
    for issue in parsed:
        issue_links = issue['_links']
        issue_id = issue['iid']
        print("issue id: " + str(issue_id))
        issue_url = "/issues/" + str(issue_id)
        if os.path.exists("comments/" + str(issue_id)):
            # comment insertion
            comment_filelist = [ filename for filename in os.listdir("comments/" + str(issue_id)) if filename.startswith('comments') ]
            sorted_filelist = sorted(comment_filelist, key = lambda x: int(re.search('comments(\d+).txt', x).group(1)))
            for c in sorted_filelist:
                print("inserting {0}".format(c))
                with open("comments/" + str(issue_id) + "/" + c, 'r', encoding="utf-8") as infile:
                    comment_data = {'body': " ".join(infile.readlines()) }
                    post_response = requests.post(base_url + project_url + issue_url + note_url, headers=headers, data=comment_data, verify=False)
                    print("comment post status: {0}".format(post_response.ok))
            # insert labels
            with open("comments/" + str(issue_id) + "/labels.txt", 'r', encoding="utf-8") as infile:
                labels_string = ""
                for line in infile:
                    labels_string = labels_string + line.strip() + ','
                if labels_string != "":
                    labels_string = labels_string[:-1]
                    data = {'labels':labels_string}
                    put_response = requests.put(base_url + project_url + issue_url, headers=headers, data=data, verify=False)
                    if not put_response.ok:
                        print("error at labels, {0}".format(data))
                        sys.exit()
            # insert milestones
            milestone_url = "/milestones"
            # first setup the milestone map
            milestone_dict = {}
            milestone_response = requests.get(base_url + project_url + milestone_url, headers=headers, params=perpage_parameters, verify=False)
            milestone_parsed = json.loads(milestone_response.text)
            for ms in milestone_parsed:
                milestone_dict[ms['title']] = ms['id']
            # now insert the milestones using the map
            with open("comments/" + str(issue_id) + "/milestone.txt", 'r', encoding="utf-8") as infile:
                milestone_name = infile.readline().strip()
                if milestone_name != "":
                    data = {'milestone_id':milestone_dict[milestone_name]}
                    put_response = requests.put(base_url + project_url + issue_url, headers=headers, data=data, verify=False)
                    if not put_response.ok:
                        print("error at milestone, {0}".format(data))
                        sys.exit()
            # insert status
            with open("comments/" + str(issue_id) + "/status.txt", 'r', encoding="utf-8") as infile:
                if infile.readline().strip() == "closed":
                    post_response = requests.put(base_url + project_url + issue_url, headers=headers, data=close_issue, verify=False)
        else:
            continue
    if 'next' in response.links.keys():
        response = requests.get(response.links['next']['url'], headers=headers, params=perpage_parameters, verify=False)
        parsed = json.loads(response.text)
    else:
        break

print("--- %s seconds ---" % (time.time() - start_time))