import requests
import json
import urllib3
import time


########################## MODIFY THESE ##########################
private_token = "ENTER YOUR PERSONAL ACCESS TOKEN HERE"
new_gitlab_home_url = "https://gitlab.com"                              # modify gitlab.com to the link to where you host your gitlab
group_nm = "ENTER YOUR GROUP NAME HERE"                                # change to "" if you don't use groups
project_name = "ENTER YOUR PROJECT PATH NAME HERE"
##################################################################

headers = {"PRIVATE-TOKEN":private_token}
base_url = new_gitlab_home_url + "/api/v4"
if group_nm != "":
    group_url = "/groups/" + group_nm
else:
    group_url = ""


print("Start")
start_time = time.time()

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


# get project_id
project_url = "/projects/"
perpage_parameters = {"per_page":"100"}

response = requests.get(base_url + group_url + project_url, headers=headers, params=perpage_parameters, verify=False)
parsed = json.loads(response.text)

links = None
project_id = -1
while True:
    for projects in parsed:
        if projects['path'] == project_name:
            project_id = projects['id']
            links = projects['_links']
            break
    if 'next' in response.links.keys():
        response = requests.get(response.links['next']['url'], headers=headers, params=perpage_parameters, verify=False)
        parsed = json.loads(response.text)
    else:
        if project_id == -1:
            print("project cannot be found")
            exit()
        break

project_url = "/projects/"+ str(project_id)


print("Deleting labels")
label_url = "/labels"

label_response = requests.get(links['labels'], headers=headers, params=perpage_parameters, verify=False)
label_parsed = json.loads(label_response.text)

while True:
    for label in label_parsed:
        label_url = "/labels/" + str(label['id'])
        notes_response = requests.delete(base_url + project_url + label_url, headers=headers, verify=False)
    if 'next' in label_response.links.keys():
        label_response = requests.get(label_response.links['next']['url'], headers=headers, verify=False)
        label_parsed = json.loads(label_response.text)
    else:
        break


print("Deleting milestones")
milestone_url = "/milestones"

ms_response = requests.get(base_url + project_url + milestone_url, headers=headers, params=perpage_parameters, verify=False)
ms_parsed = json.loads(ms_response.text)

while True:
    for ms in ms_parsed:
        milestone_url = "/milestones/" + str(ms['id'])
        ms_response = requests.delete(base_url + project_url + milestone_url, headers=headers, verify=False)
    if 'next' in ms_response.links.keys():
        ms_response = requests.get(ms_response.links['next']['url'], headers=headers, verify=False)
        ms_parsed = json.loads(ms_response.text)
    else:
        break


print("--- %s seconds ---" % (time.time() - start_time))