import requests
import json
import urllib3
import time

########################## MODIFY THESE ##########################
private_token = "ENTER YOUR PERSONAL ACCESS TOKEN HERE"
new_gitlab_home_url = "https://gitlab.com"                              # modify gitlab.com to the link to where you host your gitlab
group_nm = "ENTER YOUR GROUP NAME HERE"                                # change to "" if you don't use groups
project_name = "ENTER YOUR PROJECT PATH NAME HERE"
##################################################################

headers = {"PRIVATE-TOKEN":private_token}
base_url = new_gitlab_home_url + "/api/v4"
if group_nm != "":
    group_url = "/groups/" + group_nm
else:
    group_url = ""

print("Start label/milestone insertion")
start_time = time.time()

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


# get project_id
project_url = "/projects"
perpage_parameters = {"per_page":"100"}

response = requests.get(base_url + group_url + project_url, headers=headers, params=perpage_parameters, verify=False)
parsed = json.loads(response.text)

links = None
project_id = -1
while True:
    for projects in parsed:
        if projects['path'] == project_name:
            project_id = projects['id']
            links = projects['_links']
            break
    if 'next' in response.links.keys():
        response = requests.get(response.links['next']['url'], headers=headers, params=perpage_parameters, verify=False)
        parsed = json.loads(response.text)
    else:
        if project_id == -1:
            print("project cannot be found")
            exit()
        break

project_url = "/projects/"+ str(project_id)

print("inserting labels")
# insert labels
labels_url = "/labels"
with open("labels.txt", 'r', encoding="utf-8") as infile:
    for line in infile:
        data_list = line.strip().split(',')
        data = {'name':data_list[0], 'color':data_list[1]}
        post_response = requests.post(base_url + project_url + labels_url, headers=headers, data=data, verify=False)
        if not post_response.ok:
            print("failed at labels, {0}".format(data))
            raise SystemExit("error")

print("inserting milestones")
# insert milestones
milestone_url = "/milestones"
with open("milestone.txt", 'r', encoding="utf-8") as infile:
    for line in infile:
        data = {'title':line.strip()}
        post_response = requests.post(base_url + project_url + milestone_url, headers=headers, data=data, verify=False)
        if not post_response.ok:
            print("failed at milestones, {0}".format(data))
            raise SystemExit("error")

print("--- %s seconds ---" % (time.time() - start_time))